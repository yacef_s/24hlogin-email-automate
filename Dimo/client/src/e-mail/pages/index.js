import Head from 'next/head'
import styles from '../styles/Home.module.css'
import React from 'react'
import { Route, Link } from "react-router-dom";

export default

function Home() {
  async function handleOnSubmit(e) {
    e.preventDefault();

    const formData = {};

    Array.from(e.currentTarget.elements).forEach(field => {
      if ( !field.name ) return;
      formData[field.name] = field.value;
    });

    await fetch('/api/mail', {
      method: 'POST',
      body: JSON.stringify(formData)
    });
  }
  return (
    <div className={styles.container}>
      <Head>
        <title>Fomulaire</title>
        <meta name="description" content="Contact me for cool stuff!" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Bienvenue dans le <a href="http://127.0.0.1:3001"> formulaire</a>     
           </h1>
        <p className={styles.description}>  
          Remplissez les cases
        </p>

        <div className={styles.grid}>
          <style jsx>{`
            form {
              font-size: 1.2em;
            }
            label {
              display: block;
              margin-bottom: .2em;
            }
            input,
            textarea {
              width: 100%;
              padding: .8em;
            }
            button {
              color: white;
              font-size: 1em;
              background-color: blueviolet;
              padding: .8em 1em;
              border: none;
              border-radius: .2em;
            }
          `}</style>
          <form onSubmit={handleOnSubmit}>
          <p>
              <label htmlFor="name">Name</label>
              <input id="name" type="text" name="name" />
            </p>

            <p>
              <label htmlFor="name">Prenom</label>
              <input id="prenom" type="text" name="prenom" />
            </p>

            <p>
              <label htmlFor="name">Adresse</label>
              <input id="adresse" type="text" name="adresse" />
            </p>

            <p>
              <label htmlFor="name">Téléphone</label>
              <input id="telephone" type="text" name="telephone" />
            </p>

            <p>
              <label htmlFor="email">Email</label>
              <input id="email" type="text" name="email" />
            </p>

            <p>
              <label htmlFor="message">Type de bien</label>
              <textarea id="message" name="message" />
            </p>

            <p>
              <label htmlFor="name">Année de construction</label>
              <input id="annee" type="text" name="annee" />

            </p>
          
            <p>
              <button>Envoie adresse mail</button>
            </p>


          </form>
        </div>
      </main>
    </div>
  )
}
