const mail = require('@sendgrid/mail');

mail.setApiKey(process.env.SENDGRID_API_KEY);


export default async (req, res) => {
  const body = JSON.parse(req.body);

  const message = `
    Name: ${body.name}\r\n
    Prenom ${body.prenom}\r\n
    Adresse ${body.adresse}\r\n
    Téléphone ${body.telephone}\r\n
    Email: ${body.email} \r\n
    Type de bien: ${body.message}\r\n
    Année de construction: ${body.annee}

  `;

  const data = {
    to: 'yousou93@outlook.fr' && 'yacef_s@etna-alternance.net', // && '',
    from: 'souleymanportfolio@souleymanportfolio.com',
    subject: 'New Message!',
    text: message,
    html: message.replace(/\r\n/g, '<br>'),
  };
 

  mail.send(data);

  res.status(200).json({ status: 'Ok' });
}